use std::{fs::File};
use actix_web::{get, post, web, App, HttpServer, Responder, Result};
use actix_cors::Cors;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "PascalCase")]
struct BusStop {
    bus_stop_code: String,
    road_name: String,
    description: String,
    latitude: f64,
    longitude: f64 
}

impl BusStop {
    async fn arrival(&self) {
        next_bus(self.bus_stop_code.clone()).await;
    }
}

#[derive(Deserialize, Serialize)]
struct Point {
    latitude: f64,
    longitude: f64,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct Closest {
    bus_stop: BusStop,
    distance: f64 
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
struct Services {
    services: Vec<Service>
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
struct Service {
    service_no: String,
    operator: String,
    next_bus: Arrival,
    next_bus_2: Arrival,
}


#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
struct Arrival {
    estimated_arrival: String,
    latitude: String,
    longitude: String
}

#[derive(Debug, Deserialize)]
struct BusSearch {
    bus_stop_number: String
}

async fn next_bus(bus_stop_code: String) {
    let api = format!("http://datamall2.mytransport.sg/ltaodataservice/BusArrivalv2?BusStopCode={:}", bus_stop_code);

    let client = reqwest::Client::new();
    let res = client.get(api).header("AccountKey", "6K1LYpDgHbWKHsB59LyeyQ==").send().await;
    let res = match res {
        Ok(response) => response.json::<Services>().await,
        Err(_) => todo!(),
    };

    println!("Response: {:#?}", res);
}

fn closest(all_bus_stops: &Vec<BusStop>, current_location: Point) -> Closest {
    let mut closest = Closest {
        bus_stop: all_bus_stops[0].clone(),
        distance: 999999. 
    };

    for bs in all_bus_stops.into_iter() {
        let distance = get_distance(&current_location, Point{
            latitude: bs.latitude,
            longitude: bs.longitude,
        });

        if closest.distance > distance {
            closest.bus_stop = bs.clone();
            closest.distance = distance;
        }
    }

    closest
} 

fn get_distance(p1: &Point, p2: Point) -> f64 {
    let latd = p2.latitude - p1.latitude;
    let lngd = p2.longitude - p1.longitude;

    latd * latd + lngd * lngd
}

fn load_all_bus_stops() -> Vec<BusStop> {
    let file = File::open("all.json").unwrap();
    let bus_stops  = serde_json::from_reader(file).expect("error parsing file");

    return bus_stops;
}


#[get("/")]
async fn closest_location(point: web::Query<Point>, all_bus_stops: web::Data<Vec<BusStop>>) -> Result<impl Responder> {
    let found = closest(&all_bus_stops, point.into_inner());

    found.bus_stop.arrival().await;

    Ok(web::Json(found))
}

#[get("/next")]
async fn bus_arrival(arrival: web::Query<BusSearch>) -> Result<impl Responder> {
    next_bus(arrival.into_inner().bus_stop_number).await;
    Ok(web::Json(Point{
        latitude: 0.0,
        longitude: 0.0,
    }))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        let cors = Cors::default().allow_any_origin().send_wildcard();
        let all_bus_stops = load_all_bus_stops();

        App::new()
            .wrap(cors)
            .app_data(web::Data::new(all_bus_stops))
            .service(closest_location)
            .service(bus_arrival)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
